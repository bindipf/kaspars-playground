/**
 * API Users static class
 */
export default class ApiUsers
{
    /**
     * Get a list of users
     *
     * @param {*} action 
     */
    static getList(action) 
    {
        const timeout = 1000; // 1 second delay
        return new Promise(resolve => {
            setTimeout(() => {
                let users = [];
                for (let x = 1; x <= 28; x++) {
                    users.push({
                        id: x,
                        username: 'Johnny ' + x,
                        job: 'Employee ' + x
                    });
                }
                resolve(users);
            }, timeout);
        });
    }

    /**
     * Add a user
     *
     * @param {*} action 
     */    
        
    static add(action)
    {
        // Call some API url
    }

    /**
     * Edit a user
     *
     * @param {*} action 
     */    
        
    static edit(action)
    {
        // Call some API url
    }

    /**
     * Delete a user
     *
     * @param {*} action 
     */    
        
    static delete(action)
    {
        // Call some API url
    }
}