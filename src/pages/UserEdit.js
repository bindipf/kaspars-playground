import React from 'react';
import { connect } from "react-redux";
import { PageHeader, Form, FormGroup, Col, Button, FormControl, InputGroup, Glyphicon, HelpBlock } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { goBack } from 'react-router-redux';

/**
 * User add/edit page component
 */
export class UserEdit extends React.Component
{
    // current form type: add or edit
    form_type;

    /**
     * Constructor
     */
    constructor(props) {
        super(props);
        this.form_type = props.initialValues.id > 0 ? 'edit' : 'add';
        this.formSubmit = this.formSubmit.bind(this);
    }
    /**
     * Render
     */
    render() 
    {
        return (
            <div>
                <PageHeader>
                    {'edit' === this.form_type ? 'User edit' : 'User add'}
                </PageHeader>
                <Form horizontal onSubmit={this.props.handleSubmit(this.formSubmit)}> 
                    <Field name="username" component={UserEdit.renderUsername} />
                    <Field name="job" component={UserEdit.renderJob} />
                    <FormGroup>
                        <Col smOffset={2} sm={8}>
                            <Button type="submit" disabled={this.props.invalid || this.props.submitting}>Save User</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        )
    }
    /**
     * Render username form field
     *
     * @param {*} props 
     */
    static renderUsername(props)
    {
        return(
            <FormGroup validationState={!props.meta.touched ? null : (props.meta.error ? 'error' : 'success')}>
                <Col sm={2}>Username</Col>
                <Col sm={8}>
                    <FormControl {...props.input} id="username" type="text" placeholder="Username" />
                    <FormControl.Feedback />
                    <HelpBlock>
                        {props.meta.touched && props.meta.error ? props.meta.error : null}
                    </HelpBlock>
                </Col>
            </FormGroup>
        );
    }
    
    /**
     * Render job form field
     *
     * @param {*} props 
     */
    static renderJob(props)
    {
        return(
            <FormGroup>
                <Col sm={2}>Job</Col>
                <Col sm={8}>
                    <InputGroup>
                        <FormControl {...props.input} id="job" type="text" placeholder="Job" />
                        <InputGroup.Addon>
                            <Glyphicon glyph="briefcase" />
                        </InputGroup.Addon>
                    </InputGroup>
                </Col>
            </FormGroup>
        );
    }
    /**
     * Submit the form
     *
     * @param {*} values 
     */
    formSubmit(values)
    {
        // add/edit user in the api
        const upper_form_type = this.form_type.toUpperCase();   // ADD or EDIT
        this.props.dispatch({
            type: 'USERS_' + upper_form_type + '_SAVE',
            id: values.id,
            username: values.username,
            job: values.job
        });

        // add/edit user
        this.props.dispatch({
            type: 'USERS_' + upper_form_type,
            id: values.id,
            username: values.username,
            job: values.job,
        });

        // redirect to prev page
        this.props.dispatch(goBack());
    }
}

// decorate the form component

const UserEditForm = reduxForm({
    form: 'user_edit',
    validate: function (values) {
        const errors = {};
        if (!values.username) {
            errors.username = 'Username is required';
        }
        return errors; 
  }  
})(UserEdit);

// Export connected class

function mapStateToProps(state, own_props) {
    let form_data = {
        id: 0,
        username: '',
        job: '',
    };
    for (let user of state.users.list) {
        if (user.id === Number(own_props.params.id)) {
            form_data = user;
            break;
        }
    }
    return {
        initialValues: form_data,
    };
}

export default connect(mapStateToProps)(UserEditForm);

