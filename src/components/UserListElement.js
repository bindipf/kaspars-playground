import React from 'react';
import { Button, Glyphicon } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router';

/**
 * User list element component
 */
class UserListElement extends React.Component
{
    /**
     * Constructor
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.modalDeleteShow = this.modalDeleteShow.bind(this);
    }
    /**
     * Render
     */
    render() {
        const user = this.props.user;
        return (
            <tr>
                <td># {user.id}</td>
                <td>{user.username}</td>
                <td>{user.job}</td>
                <td>
                    <Link to={'/user-edit/' + user.id}>
                        <Button bsSize="xsmall">
                            Edit <Glyphicon glyph="edit" />
                        </Button>
                    </Link>
                </td>
                <td>
                    <Button bsSize="xsmall" data-id={user.id} data-username={user.username} onClick={this.modalDeleteShow}>
                        Delete <Glyphicon glyph="remove-circle" />
                    </Button>
                </td>
            </tr>
        )
    }
    /**
     * Promt to delete the user
     *
     * @param {*} event 
     */
    modalDeleteShow(event)
    {
        const user_id = Number(event.target.dataset.id);
        const username = event.target.dataset.username;
        this.props.dispatch({
            type: 'USERS_MODAL_DELETE_SHOW',
            id: user_id,
            username: username,
        });
    }
}

// Export connected class
export default connect() (UserListElement);