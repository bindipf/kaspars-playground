import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { connect } from 'react-redux';

/**
 * User delete component
 */
export class UserDelete extends React.Component
{
    constructor(props)
    {
        super(props);

        this.modalDeleteHide = this.modalDeleteHide.bind(this);
        this.userDelete = this.userDelete.bind(this);
    }

    /**
     * Render
     */
    render()
    {
        return (
            <Modal show={this.props.modal_delete.show}>
                <Modal.Header>
                    <Modal.Title>
                        Are you sure you want to delete &nbsp;
                        <strong>{this.props.modal_delete.username}</strong>?
                    </Modal.Title>
                </Modal.Header>

                <Modal.Footer>
                    <Button onClick={this.modalDeleteHide}>No</Button>
                    <Button bsStyle="primary" onClick={this.userDelete}>Yes</Button>
                </Modal.Footer>
            </Modal> 
        );
    }
    /**
     * Close the delete modal
     *
     * @param {*} event 
     */
    modalDeleteHide(event)
    {
        this.props.dispatch({
            type: 'USERS_MODAL_DELETE_HIDE',
        });
    }

    /**
     * Delete a user
     *
     * @param {*} event 
     */
    userDelete(event)
    {
        // Delete user with api
        this.props.dispatch({
            type: 'USERS_DELETE_SAVE',
            id: this.props.modal_delete.id,
        });
        // Delete user from state
        this.props.dispatch({
            type: 'USERS_DELETE',
            id: this.props.modal_delete.id, // taken from the modal state
        });

        // Hide modal
        this.props.dispatch({
            type: 'USERS_MODAL_DELETE_HIDE',
        });
    }
}

// export the connected class
function mapStateToProps(state) {
    return {
        modal_delete: (state.users.modal && state.users.modal.list_delete) ? state.users.modal.list_delete : {
            show: false,
            id: 0,
            username: '',
        },
    };
}
export default connect(mapStateToProps)(UserDelete);