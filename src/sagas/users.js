import { call, put } from 'redux-saga/effects';
import ApiUsers from '../api/users';
/**
 * Fetch users list
 *
 * @param {*} action 
 */
export function* usersFetchList(action) {
    // call the API to get the users
    const users = yield call(ApiUsers.getList);

    // dispatch success action
    yield put({
        type: 'USERS_FETCH_LIST_SUCCESS',
        users: users,
    });
}

/**
 * Add a user
 * @param {*} action 
 */
export function* usersAddSave(action) {
    yield call(ApiUsers.add, action);
}

/**
 * Edit a user
 * @param {*} action 
 */
export function* usersEditSave(action) {
    yield call(ApiUsers.edit, action);
}

/**
 * Delete a user
 * @param {*} action 
 */
export function* usersDeleteSave(action) {
    yield call(ApiUsers.delete, action);
}