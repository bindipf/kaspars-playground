import React from 'react';
import { shallow } from 'enzyme';
import assert from 'assert';

import { UserDelete } from '../../src/components/UserDelete';

// Unit tests for the App component
describe('UserDelete componenet', () => {
    describe('render()', () => {
        it('should render the componenet', () => {
            const props = {
                modal_delete: {
                    show: false,
                    id: 0,
                    username: '',
                }
            }
            const wrapper = shallow(<UserDelete {...props} />);
            assert.equal(wrapper.find('Modal').length, 1);
            assert.equal(wrapper.find('Button').length, 2);
            
        });
    });
});