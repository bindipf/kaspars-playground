import React from 'react';
import { shallow } from 'enzyme';
import assert from 'assert';
import Menu from '../../src/components/Menu';

// Unit tests for the App component
describe('Menu componenet', () => {
    describe('render()', () => {
        it('should render the componenet', () => {
            const wrapper = shallow(<Menu/>);
            assert.equal(wrapper.find('IndexLinkContainer').length, 1);
            assert.equal(wrapper.find('LinkContainer').length, 1);
            assert.equal(wrapper.find('NavItem').length, 2);
            
        });
    });
});