import React from 'react';
import { shallow } from 'enzyme';
import assert from 'assert';
import App from '../../src/components/App';

// Unit tests for the App component
describe('App componenet', () => {
    describe('render()', () => {
        it('should render the componenet', () => {
            const wrapper = shallow(<App />);
            assert.equal(wrapper.find('.container').length, 1);
            assert.equal(wrapper.find('.row').length, 2);
            
        });
    });
});