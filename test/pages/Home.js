import React from 'react';
import { shallow } from 'enzyme';
import assert from 'assert';

import Home from '../../src/pages/Home';

// Unit tests for the Home component
describe('Home componenet', () => {
    describe('render()', () => {
        it('should render the componenet', () => {
            const wrapper = shallow(<Home/>);
            assert.equal(wrapper.find('div').length, 1);            
        });
    });
});