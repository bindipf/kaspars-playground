import React from 'react';
import { shallow } from 'enzyme';
import assert from 'assert';

import NotFound from '../../src/pages/NotFound';

// Unit tests for the NotFound component
describe('NotFound componenet', () => {
    describe('render()', () => {
        it('should render the componenet', () => {
            const wrapper = shallow(<NotFound/>);
            assert.equal(wrapper.find('h4').children().text(), 'Page not found');
        });
    });
});