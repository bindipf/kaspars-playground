import React from "react";
import { shallow } from "enzyme";
import assert from "assert";

import { UserEdit } from "../../src/pages/useredit";

describe('UserEdit page', () => {

  it('should exists', () => {
    assert.notEqual(typeof UserEdit, 'undefined');
  });

  describe('render()', () => {

    it('should render the add user form', () => {
      const props = {
        initialValues: {
          id: 0
        },
        handleSubmit: () => { },
        invalid: true,
        submitting: false,
      };
      const wrapper = shallow(<UserEdit {...props} />);
      assert.equal(wrapper.find('PageHeader').children().text(), 'User add');
      assert.equal(wrapper.find('Button').prop('disabled'), true);
    });

    it('should render the edit user form', () => {
      const props = {
        initialValues: {
          id: 1
        },
        handleSubmit: () => { },
        invalid: false,
        submitting: false,
      };
      const wrapper = shallow(<UserEdit {...props} />);
      assert.equal(wrapper.find('PageHeader').children().text(), 'User edit');
      assert.equal(wrapper.find('Button').prop('disabled'), false);
    });

  });

});